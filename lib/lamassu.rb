# frozen_string_literal: true

require 'dry/configurable'
require 'lamassu/namespace_resolver'

# Lamassu is container and policy based authorization framework
module Lamassu
  extend Dry::Configurable

  setting :namespace_resolver, Lamassu::NamespaceResolver.new, reader: true
end

require 'lamassu/guardian'
require 'lamassu/policy'

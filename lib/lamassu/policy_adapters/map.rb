# frozen_string_literal: true

require 'dry/monads/result'

module Lamassu
  module PolicyAdapters
    # Policy Adapter for a callable wrapping returned value to Success
    class Map
      include Dry::Monads::Result::Mixin

      attr_reader :policy

      def initialize(policy)
        @policy = policy
      end

      def call(*args)
        value = policy.call(*args)

        Success(value)
      end
    end
  end
end

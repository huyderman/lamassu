# frozen_string_literal: true

require 'dry/inflector'

module Lamassu
  # Default namespace resolver used for scopes in Lamassu
  #
  # If target is a symbol or string, it is used as is. If passed a Module,
  # it is translated underscorized string. For any other object, it resolves
  # the targets class and it's translated as above.
  class NamespaceResolver
    def initialize(inflector: Dry::Inflector.new)
      @inflector = inflector
    end

    def call(target)
      case target
      when Module
        coerce_module(target)
      when String, Symbol
        coerce_string(target)
      else
        coerce_object(target)
      end
    end

    private

    def coerce_string(target)
      target.to_s
    end

    def coerce_object(target)
      @inflector.underscore(target.class)
    end

    def coerce_module(target)
      @inflector.underscore(target)
    end
  end
end

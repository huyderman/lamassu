# frozen_string_literal: true

require 'dry/monads/result'

module Lamassu
  # Base module to be included in policy objects
  module Policy
    def self.included(klass)
      klass.class_eval do
        include Dry::Monads::Result::Mixin
      end
    end
  end
end

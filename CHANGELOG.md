# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.2.0] - 2019-02-15

## Changed

- Bumped required versions of `dry-configurable` and `dry-container`.
  These now require minimum of Ruby 2.3, which was already a requirment of `lamassu`.

## [v0.1.1] - 2018-09-16

## Fixed

- Gem metadata

## [v0.1.0] - 2018-09-14

### Added

- Initial implementation of Lamassu

[Unreleased]: https://gitlab.com/huyderman/lamassu/compare/v0.2.0...HEAD
[v0.2.0]: https://gitlab.com/huyderman/lamassu/compare/v0.1.1...v0.2.0
[v0.1.1]: https://gitlab.com/huyderman/lamassu/compare/v0.1.0...v0.1.1
[v0.1.0]: https://gitlab.com/huyderman/lamassu/compare/v0.0.0...v0.1.0

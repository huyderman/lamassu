# frozen_string_literal: true

require 'lamassu/namespace_resolver'

RSpec.describe Lamassu::NamespaceResolver do
  let(:namespace_resolver) { described_class.new }

  describe '#call' do
    context 'when passed a module' do
      subject { namespace_resolver.call(String) }

      it { is_expected.to be_a String }
      it { is_expected.to eq 'string' }
    end
    context 'when passed a String' do
      let(:string) { 'foo' }
      subject { namespace_resolver.call(string) }

      it { is_expected.to be_a String }
      it { is_expected.to eq 'foo' }
    end
    context 'when passed a Symbol' do
      let(:symbol) { :foo }
      subject { namespace_resolver.call(symbol) }

      it { is_expected.to be_a String }
      it { is_expected.to eq 'foo' }
    end
    context 'when passed an Object' do
      let(:object) { Time.now }
      subject { namespace_resolver.call(object) }

      it { is_expected.to be_a String }
      it { is_expected.to eq 'time' }
    end
  end
end

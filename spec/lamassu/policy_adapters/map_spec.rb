# frozen_string_literal: true

require 'dry/monads/result'
require 'lamassu/policy_adapters/check'

RSpec.describe Lamassu::PolicyAdapters::Map do
  describe '#call' do
    context 'initialized with a proc returning a value' do
      let(:value) { 42 }
      let(:map) { described_class.new(proc { value }) }

      context 'the return value' do
        subject { map.call }

        it { is_expected.to be_a Dry::Monads::Result::Success }
        it { is_expected.to have_attributes(value!: value) }
      end
    end

    context 'initialized with a proc transforming a value' do
      let(:value) { 'foo' }
      let(:map) { described_class.new(:upcase.to_proc) }

      context 'the return value' do
        subject { map.call(value) }

        it { is_expected.to be_a Dry::Monads::Result::Success }
        it { is_expected.to have_attributes(value!: 'FOO') }
      end
    end
  end
end

# frozen_string_literal: true

Gem::Specification.new do |spec|
  version_file = File.expand_path('VERSION', __dir__)
  version = File.read(version_file).lines.first.chomp

  spec.name    = 'lamassu'
  spec.version = version
  spec.authors = ['Jo-Herman Haugholt']
  spec.email   = ['johannes@huyderman.com']

  spec.summary  = 'Autorization gem based on policy objects and dry-container'
  spec.homepage = 'https://gitlab.com/huyderman/lamassu'
  spec.license  = 'MIT'
  spec.metadata = {
    'documentation_uri' => 'https://gitlab.com/huyderman/lamassu/blob/master/README.md',
    'changelog_uri' => 'https://gitlab.com/huyderman/lamassu/blob/master/CHANGELOG.md',
    'bug_tracker_uri' => 'https://gitlab.com/huyderman/lamassu/issues'
  }

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.3'

  spec.add_runtime_dependency 'dry-configurable', '~> 0.8.0'
  spec.add_runtime_dependency 'dry-container', '~> 0.7.0'
  spec.add_runtime_dependency 'dry-inflector', '~> 0.1.1'
  spec.add_runtime_dependency 'dry-monads', '~> 1.0'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'fasterer', '~> 0.4.2'
  spec.add_development_dependency 'mutant', '~> 0.8.0'
  spec.add_development_dependency 'mutant-rspec', '~> 0.8.0'
  spec.add_development_dependency 'pry', '~> 0.12.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'reek', '~> 5.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.52'
  spec.add_development_dependency 'simplecov', '~> 0.16'
end
